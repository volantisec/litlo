//
//  ViewController.swift
//  Litlo
//
//  Created by Gerson Noboa on 4/13/17.
//  Copyright © 2017 Volantis. All rights reserved.
//

import UIKit
import Onboard

class ViewController: UIViewController {
    
    var onboard: OnboardingViewController?
    var login: LoginViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (self.onboard == nil){
            createOnboard()
            self.present(self.onboard!, animated: true, completion: nil)
        }
        
    }
    
    

    func createOnboard(){
        let view1 = OnboardingContentViewController(title: "Hello Twilio", body: "This is my small app called Litlo", image: nil, buttonText: nil, action: nil)
        
        let view2 = OnboardingContentViewController(title: "Litlo is...", body: "...an app that uses your services to connect students around the world and in the same classroom.", image: nil, buttonText: nil, action: nil)
        
        let view3 = OnboardingContentViewController(title: "I hope you like it", body: "It's rough around the edges but UT consumes anything that resembles a little bit of free time.", image: nil, buttonText: "Start") {
            self.onboard?.dismiss(animated: true, completion: nil)
            
            self.createLogin()
            self.present(self.login!, animated: true, completion: nil)
        }
        
        self.onboard = OnboardingViewController(backgroundImage: UIImage(named: "bgOnboard.jpg"), contents: [view3])
        self.onboard?.shouldMaskBackground = false
        
    }
    
    func createLogin(){
        self.login = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

