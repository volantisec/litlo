//
//  FriendsViewController.swift
//  Litlo
//
//  Created by Gerson Noboa on 4/13/17.
//  Copyright © 2017 Volantis. All rights reserved.
//

import UIKit

class FriendsViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 7 : 5
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Friends" : "Groups"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")!
        
        if (indexPath.section == 0){
            cell.textLabel?.text = "Friend \(indexPath.row)"
            cell.detailTextLabel?.text = "Class \(indexPath.row+1)"
        }
        else{
            cell.textLabel?.text = "Group \(indexPath.row)"
            cell.detailTextLabel?.text = "Class \(indexPath.row+1)"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
