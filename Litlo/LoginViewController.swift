//
//  LoginViewController.swift
//  Litlo
//
//  Created by Gerson Noboa on 4/13/17.
//  Copyright © 2017 Volantis. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginPressed(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
        
        changeRootViewController()
        	
    }
    
    func changeRootViewController(){
        let tabBar = self.storyboard?.instantiateViewController(withIdentifier: "tabBarController") as! TabBarViewController
        
        let window = self.view.window
        
        let snapshot = window?.snapshotView(afterScreenUpdates: true)!
        
        self.view.addSubview(snapshot!)
        
        window?.rootViewController = tabBar
        
        
        UIView.animate(withDuration: 0.5, animations: { 
            snapshot?.layer.opacity = 0
            snapshot?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
        }) { (finished) in
            snapshot?.removeFromSuperview()
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
